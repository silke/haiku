# Generated Haiku
Website containing the results of a haiku study, and a quiz allowing you to test yourself!

## Building
This website is setup with [Hugo](https://gohugo.io/),
a simple static website engine.
With Hugo installed, building the site is simple. Just run:

    hugo

That's it!
