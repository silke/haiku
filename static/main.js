/**
 * Copyright 2018 - Silke Hofstra
 *
 * Licensed under the EUPL
 *
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */

let current;
let doneHaiku = [];
let haiku = [];
let correct = 0;

function showHaiku() {
    "use strict";
    current = haiku.splice(Math.floor(Math.random()*haiku.length), 1)[0];
    document.getElementById("haiku").innerHTML = current.haiku;
}

function showResult() {
    "use strict";

    // Hide buttons
    let buttons = document.getElementsByClassName("generate");
    for (let b of buttons) {
        b.setAttribute("style", "display: none");
    }

    // Summary
    document.getElementById("haiku").innerHTML =
        "You have guessed " + correct + "/" + doneHaiku.length + " correctly!";

    // Details
    const gen = document.getElementById("generator");
    for (let h of doneHaiku) {
        let p = document.createElement("p");
        p.innerHTML = "<em>Author: " + h.author + "</em><br/>" + h.haiku;
        p.classList.add("guess");
        p.classList.add((h.author === h.guess) ? "correct" : "incorrect");
        gen.appendChild(p);
    }
}

function next() {
    "use strict";

    if (current.author === current.guess) {
        correct++;
    }

    doneHaiku.push(current);

    if (haiku.length === 0) {
        showResult();
    } else {
        showHaiku();
    }
}

function human() {
    "use strict";
    current.guess = "Human";
    next();
}

function machine() {
    "use strict";
    current.guess = "Machine";
    next();
}

function init() {
    "use strict";

    fetch("../haiku.json")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            haiku = data;
            showHaiku();
        });
}

try {
    init();
}
catch(error) {
    document.getElementById("error").style.display = "block";
    throw error;
}
